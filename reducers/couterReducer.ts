import { createSlice } from "@reduxjs/toolkit";
import type { PayloadAction } from '@reduxjs/toolkit';

type  initalStateType  = {
  number: number
}
const initialState: initalStateType = {
  number: 0,
};

export const counterSlice = createSlice({
  name: 'counter',
  initialState,
  reducers: {
    increment: (state) => {
      state.number += 1;
    },
    incrementByValue: (state, action: PayloadAction<number>) => {
      state.number += action.payload
    }
  }
});

export const { increment, incrementByValue } = counterSlice.actions;
export default counterSlice.reducer;