import { call, put, takeEvery, takeLatest } from 'redux-saga/effects';
import { getNewsList } from '../pages/api/news';
import {newsRequest, newsList} from "../reducers/newsReducer";
import newsService from '../services/newsService';

function* fetchList(): any {
  try {
    // if(!process.env.NEXT_PUBLIC_APP_API_URL) {
    //   throw new Error('env key missing')
    // }
    // const apiUrl: string = process.env.NEXT_PUBLIC_APP_API_URL;
    const {data} = yield call(newsService.get, `/api/news`);
    yield put(newsList(data))
  } catch (error) {
    console.log(error)
  }
}


function* newsSaga() {
  yield takeLatest(newsRequest, fetchList)
};

export default newsSaga;