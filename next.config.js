/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  swcMinify: true,
  images: {
    loader: 'default',
    domains: ['static01.nyt.com', 'www.reuters.com', 'cdn.mos.cms.futurecdn.net', 'i.guim.co.uk', 'a.fsdn.com', 'i.insider.com', 'cdn.siasat.com', 'venturebeat.com', 's.yimg.com' ,'cdn.vox-cdn.com', 'daringfireball.net', 'sector7.computest.nl', 'blog.pragmaticengineer.com' ,'heise.cloudimg.io' , 'i0.wp.com', 'o.aolcdn.com', 'photos5.appleinsider.com', 'assets.media-platform.com', 'www.journaldugeek.com', 'i.blogs.es', 'www.xda-developers.com', 'www.techdirt.com', 'assets.bwbx.io', 'images.macrumors.com', 'cdn.lesnumeriques.com', 'images.frandroid.com', 'wwwhatsnew.com', 'srad.jp', 'static.slickdealscdn.com', 'static.cnbetacdn.com', 'www.highsnobiety.com', 'ichef.bbci.co.uk', 'fdn.gsmarena.com', 'wired.jp' , 'assets.reedpopcdn.com', 'g.foolcdn.com' , 'cms.prod.nypr.digital', 'www.appbank.net', 'img.welt.de', 'www.numerama.com', 'livedoor.blogimg.jp', 'www.basicthinking.de', 'www.ghacks.net', 'cdn2.psychologytoday.com', 'imageio.forbes.com', 'www.slashgear.com', 'www.danshihack.com', 'antyweb.pl', 'img.olhardigital.com.br', 'static.mydealz.de'],
  }
}

module.exports = nextConfig
