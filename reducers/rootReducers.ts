import { combineReducers } from "@reduxjs/toolkit";
import newsReducer from "./newsReducer";
import conterReducer from "./couterReducer";

const rootReducers = combineReducers({
  counter: conterReducer,
  news: newsReducer
});

export default rootReducers;