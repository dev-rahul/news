import axios from "axios"
import { NextApiRequest, NextApiResponse } from "next";
import newsService from "../../services/newsService";

export const getNewsList = async() => {
  if(!process.env.NEXT_PUBLIC_APP_API_URL) {
    throw new Error('env key missing')
  }
  const apiUrl: string = process.env.NEXT_PUBLIC_APP_API_URL;
  try{
    const data = await newsService.get(apiUrl);
    return data.data;
  } catch(e) {
    console.log(e)
    return e
  }
};

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  // console.log(req.query?.q)
  const result  = await getNewsList();
  res.json(result);
}