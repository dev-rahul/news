import { all, fork } from "redux-saga/effects";
import newsSaga from "./newsSaga";

export const sagas = [newsSaga];

export default function* root() {
  yield all(sagas.map((saga) => fork(saga)))
}