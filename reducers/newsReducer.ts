import { createSlice } from "@reduxjs/toolkit";
import type { PayloadAction } from '@reduxjs/toolkit';

type  initalStateType  = {
  list: any,
  loading: boolean
}
const initialState: initalStateType = {
  list: undefined,
  loading: false
};

export const newsSlice = createSlice({
  name: 'news',
  initialState,
  reducers: {
    newsRequest: (state) => {
      state.loading = true
    },
    newsList: (state, action: PayloadAction<any>) => {
      state.list = action.payload;
      state.loading = false;
      return state
    }
  }
});

export const { newsList, newsRequest } = newsSlice.actions;
export const getNewsItems = (state: any) => {
  return state.news.list
}

export default newsSlice.reducer;