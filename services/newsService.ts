import axios from "axios";

const newsService = axios.create({
  headers: {
    'X-RapidAPI-Key': '5e26f4499amsh1286ae2e37af707p1c2516jsn360297514c7f',
    'X-RapidAPI-Host': 'movie-database-alternative.p.rapidapi.com'
  }
});

export default newsService;

// mobx
// zustand
// APISaues
// ts
// 